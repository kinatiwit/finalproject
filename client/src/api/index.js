import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:5000/api",
});

export const insertLog = (payload) => api.post(`/log`, payload);
export const getAllLogs = () => api.get(`/logs`);
export const updateLogById = (id, payload) => api.put(`/log/${id}`, payload);
export const deleteLogById = (id) => api.delete(`/log/${id}`);

const apis = {
  insertLog,
  getAllLogs,
  updateLogById,
  deleteLogById,
};

export default apis;
