import React, { useState, useEffect } from "react";
import apis from "./api/index";

const Modal = ({ isActive, setActive }) => {
  const initialLogState = {
    distance: "",
    date: "",
    time: "",
  };

  const [log, setLog] = useState(initialLogState);
  const [disable_add, setDisable] = useState(true);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setLog({ ...log, [name]: value });
  };

  const addLog = async () => {
    await apis.insertLog(log).then((res) => {
      setLog(initialLogState);
      document.getElementById("form").reset();
      setDisable(true);
      window.location.reload();
    });
  };

  useEffect(() => {
    console.log(log);
    if (!!log.distance && !!log.date && !!log.time) {
      setDisable(false);
    }
  });
  return (
    <div>
      <div className={"modal " + isActive}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Add Running Log</p>
            <button
              className="delete"
              aria-label="close"
              onClick={() => {
                setActive(" ");
              }}
            ></button>
          </header>
          <section className="modal-card-body">
            <div className="field">
              <form id="form">
                <label className="label">Distance(km)</label>
                <input
                  className="input"
                  type="number"
                  placeholder="Distance"
                  onChange={handleInputChange}
                  name="distance"
                ></input>
                <label className="label">Date</label>
                <input
                  type="date"
                  onChange={handleInputChange}
                  name="date"
                ></input>
                <label className="label">Total Time</label>
                <div>
                  <input
                    type="text"
                    className="input html-duration-picker"
                    placeholder="Time"
                    onChange={handleInputChange}
                    name="time"
                  ></input>
                </div>
              </form>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button
              className="button is-success"
              disabled={disable_add}
              onClick={addLog}
            >
              Add Log
            </button>
          </footer>
        </div>
      </div>
    </div>
  );
};

export default Modal;
