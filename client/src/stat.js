import React, { useState, useEffect } from "react";
import Card from "./card";
import apis from "./api/index";

const Stat = ({ logData }) => {
  var totalDistance = 0;
  var totaltimeInSecond = 0;
  logData.map((e) => {
    totaltimeInSecond +=
      parseInt(e.time.split(":")[0]) * 3600 +
      parseInt(e.time.split(":")[1]) * 60 +
      parseInt(e.time.split(":")[2]);

    totalDistance += parseFloat(e.distance);
  });

  var hours = Math.floor(totaltimeInSecond / 3600);
  var minutes = Math.floor((totaltimeInSecond - hours * 3600) / 60);
  var seconds = totaltimeInSecond - hours * 3600 - minutes * 60;

  var totalTime =
    hours + " hours " + minutes + " minutes " + seconds + " second";

  var speed = ((totalDistance * 3600) / totaltimeInSecond).toFixed(2) + " km/h";

  return (
    <div>
      <div class="columns">
        <Card title="Speed" content={speed} />
        <Card title="Total Distance" content={totalDistance + " km"} />
        <Card title="Total Time" content={totalTime} />
      </div>
    </div>
  );
};

export default Stat;
