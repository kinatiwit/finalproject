import React from "react";

const Header = ({ openModal }) => {
  return (
    <div className="mt-5 level">
      <h1 className="title is-size-1 level-left">Running Tracker </h1>
      <a className="button is-success level-center" onClick={() => openModal()}>
        <span className="icon is-small">
          <i className="fas fa-plus"></i>
        </span>
      </a>
    </div>
  );
};

export default Header;
