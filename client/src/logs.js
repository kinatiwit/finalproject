import React, { useState, useEffect } from "react";
import Log from "./log";

const Logs = ({ logData }) => {
  return (
    <table className="table ">
      <thead>
        <tr>
          <th>Date</th>
          <th>Distance(km)</th>
          <th>Time</th>
        </tr>
      </thead>
      <tbody>
        {logData.map((data) => {
          return <Log logData={data} />;
        })}
      </tbody>
    </table>
  );
};

export default Logs;
