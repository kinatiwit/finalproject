import React from "react";

const Card = ({ title, content }) => {
  return (
    <div class="column">
      <div>
        <div className="card">
          <div className="card-content">
            <div className="content level-item has-text-centered">
              <h1 class="title">{title}</h1>
            </div>
            <div className="content level-item has-text-centered">
              <h1 class="title">{content}</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
