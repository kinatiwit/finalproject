import React from "react";
import apis from "./api/index";

const Log = ({ logData }) => {
  const deleteLog = (id) => {
    if (window.confirm(`Do tou want to delete this log permanently?`)) {
      console.log(id);
      apis.deleteLogById(id).then((res) => {
        window.location.reload();
      });
    }
  };
  return (
    <tr>
      <td>{logData.date}</td>
      <td>{logData.distance}</td>
      <td>{logData.time}</td>
      <td>
        <i className="fas fa-trash" onClick={() => deleteLog(logData._id)}></i>
      </td>
    </tr>
  );
};

export default Log;
