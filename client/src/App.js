import React, { useState, useEffect } from "react";

import apis from "./api/index";

import Header from "./header";
import Logs from "./logs";
import Modal from "./modal";
import Stat from "./stat";

import "./App.css";

import { BrowserRouter } from "react-router-dom";

var _ = require("lodash");

const App = () => {
  const [logData, setLogdata] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await apis.getAllLogs();
      setLogdata(result.data.data);
      console.log(result.data.data);
    };

    fetchData();
  }, []);

  const [active, setActive] = useState(" ");

  const openModal = () => {
    setActive("is-active");
  };

  return (
    <div className="container is-fluid ">
      <div className=" box main_box px-6">
        <Header openModal={openModal} />
        <Logs logData={logData} />
        <Stat logData={logData} />
      </div>
      <Modal isActive={active} setActive={setActive} />
    </div>
  );
};

export default App;
