const express = require("express");

const LogCtrl = require("../controllers/log-ctrl");

const router = express.Router();

router.post("/log", LogCtrl.createLog);
router.put("/log/:id", LogCtrl.updateLog);
router.delete("/log/:id", LogCtrl.deleteLog);
router.get("/log/:id", LogCtrl.getLogById);
router.get("/logs", LogCtrl.getLogs);

module.exports = router;
