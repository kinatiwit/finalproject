const Log = require("../models/log-model");

createLog = (req, res) => {
  const body = req.body;

  if (!body) {
    return res.status(400).json({
      success: false,
      error: "You must provide a log",
    });
  }

  const log = new Log(body);

  if (!log) {
    return res.status(400).json({ success: false, error: err });
  }

  log
    .save()
    .then(() => {
      return res.status(201).json({
        success: true,
        id: log._id,
        message: "Log created!",
      });
    })
    .catch((error) => {
      return res.status(400).json({
        error,
        message: "Log not created!",
      });
    });
};

updateLog = async (req, res) => {
  const body = req.body;

  if (!body) {
    return res.status(400).json({
      success: false,
      error: "You must provide a body to update",
    });
  }

  Log.findOne({ _id: req.params.id }, (err, log) => {
    if (err) {
      return res.status(404).json({
        err,
        message: "Log not found!",
      });
    }
    log.distance = body.distance;
    log.time = body.time;
    log.date = body.date;
    log
      .save()
      .then(() => {
        return res.status(200).json({
          success: true,
          id: log._id,
          message: "Log updated!",
        });
      })
      .catch((error) => {
        return res.status(404).json({
          error,
          message: "Log not updated!",
        });
      });
  });
};

deleteLog = async (req, res) => {
  await Log.findOneAndDelete({ _id: req.params.id }, (err, log) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }

    if (!log) {
      return res.status(404).json({ success: false, error: `Log not found` });
    }

    return res.status(200).json({ success: true, data: log });
  }).catch((err) => console.log(err));
};

getLogById = async (req, res) => {
  await Log.findOne({ _id: req.params.id }, (err, log) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }

    if (!log) {
      return res.status(404).json({ success: false, error: `Log not found` });
    }
    return res.status(200).json({ success: true, data: log });
  }).catch((err) => console.log(err));
};

getLogs = async (req, res) => {
  await Log.find({}, (err, logs) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }
    if (!logs.length) {
      return res.status(404).json({ success: false, error: `Log not found` });
    }
    return res.status(200).json({ success: true, data: logs });
  }).catch((err) => console.log(err));
};

module.exports = {
  createLog,
  updateLog,
  deleteLog,
  getLogById,
  getLogs,
};
