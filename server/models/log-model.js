const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Log = new Schema(
  {
    distance: { type: String, required: true },
    time: { type: String, required: true },
    date: { type: String, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("logs", Log);
